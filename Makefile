all:
	latexmk -pdflua Demo.tex

clean:
	rm *.aux *.fdb_latexmk *.fls *.log *.blg *.out *.toc *.run.xml *.bbl *.bcf
	rm -f Demo.pdf
