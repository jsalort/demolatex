# [Fichier PDF (salort.eu)](https://jsalort.pages.salort.eu/demolatex/Demo.pdf)
# [Fichier PDF (framagit)](https://jsalort.frama.io/demolatex/Demo.pdf)

Démonstration d'utilisation de GitLab pour un projet LaTeX:

* pre-commit hook pour valider un style de code (seulement dans les dépôts
locaux);
* compilation automatique sur le serveur et déploiement sur GitLab Pages
 
Limitations:

* intégration en continue basée sur GitLab Runner qui doit être autorisé
manuellement pour chaque projet (salort.eu);
* Utilisation de l'image docker sur framagit

## Pre-commit hooks

Il s'agit de vérifications automatiques qui démarrent lorsqu'on effectue la
commande
```bash
$ git commit -m "description"
```
dans le dépôt en local.

Elles sont configurées dans le fichier `.pre-commit-config.yaml` et s'installe
dans le dépôt local avec la commande:
```bash
$ pre-commit install
```
J'utilise `latexindent` et `chktex` en utilisant mon propre hook
hébergé sur ce [dépôt public](https://gitlab.salort.eu/jsalort/latexhook)


## Règles de compilation

J'utilise simplement un fichier `Makefile` qui lui-même utilise le script
`latexmk` pour compiler le bon nombre de fois, et appeler bibtex ou biber si
nécessaire, etc.

Pour compiler en local:
```bash
$ make
```

## Compilation sur le serveur

Les tâches executées sur le serveur pour chaque commit sont définies dans le
fichier `.gitlab-ci.yml`
Ici, j'utilise une tâche `pages` basée sur une image docker. J'appelle
`make`. Ensuite, tous les fichiers que l'on souhaite déployer sur GitLab Pages
doivent être copiés dans un répertoire `public`.
GitLab execute alors une tâche de déploiement après la tâche de compilation.

C'est le même principe que pour le déploiement d'une documentation Sphinx.
